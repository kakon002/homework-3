﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Hierarchical
{
    public partial class Cake : ContentPage
    {
        public Cake()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Cake)}:  ctor");
            InitializeComponent();
        }
    }
}
