﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Hierarchical
{
    public partial class Cookie : ContentPage
    {
        public Cookie(string amountOfCookie)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Cookie)}:  ctor");
            InitializeComponent();

            addressLabel.Text = amountOfCookie;
        }
    }
}
