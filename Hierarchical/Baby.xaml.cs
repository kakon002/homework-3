﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Hierarchical
{
    public partial class Baby : ContentPage
    {
        public Baby()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Baby)}:  ctor");
            InitializeComponent();
        }
    }
}
