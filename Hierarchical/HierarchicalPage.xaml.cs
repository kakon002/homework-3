﻿using Xamarin.Forms;
using System.Diagnostics;
using System;

namespace Hierarchical
{
    public partial class HierarchicalPage : ContentPage
    {
        public HierarchicalPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HierarchicalPage)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void Cookies_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Cookies_Clicked)}");
            string response = await DisplayActionSheet("How many batches do you want to make?", "Cancel",
                                                       null,"1","2","3","4");
            if (response.Equals("1", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new Cookie("YUM YUM YUM!"));
            } 
            else if (response.Equals("2", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new Cookie("You will need to double the ingredients!"));
            } 
            else if (response.Equals("3", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new Cookie("You will need to triple the ingredients!"));
            } 
            else if (response.Equals("4", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new Cookie("You will need to quadruple the ingredients!"));
            }
        }

        void Cake_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Cake_Clicked)}");
            Navigation.PushAsync(new Cake());
        }

        async void Baby_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Baby_Clicked)}");
            bool usersResponse = await DisplayAlert("WARNING!",
                                                   "Do you really want to know the ingredients for a Dutch Baby?",
                                                    "No", "Yes");

            if (usersResponse == false)
            {
                await Navigation.PushAsync(new Baby());
            }

        }
    }
}
